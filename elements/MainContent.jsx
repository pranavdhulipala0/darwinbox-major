"use client"
import AreaChartComponent from "@/components/recharts/AreaChartComponent";
import AreaLineChartComponent from "@/components/recharts/AreaLIneChartComponent";
import BarChartComponent from "@/components/recharts/BarChartComponent";
import LineChartComponent from "@/components/recharts/LineChartComponent";
import PieChartComponent from "@/components/recharts/PieChartComponent";
import RadarChartComponent from "@/components/recharts/RadarChartComponent";
import ScatterChartComponent from "@/components/recharts/ScatterChartComponent";
import React from "react";
import { NavigationBar } from "./NavigationBar";
import TRGroupBar from "@/components/tremor-charts/TRGroupBar";
import { TRCard } from "@/components/tremor-components/TRCard";

const Summary = () => {
  return (
    <div className="h-full w-full">
      <div className="py-3 rounded-lg bg-white dark:bg-neutral-900 p-4 mx-2 justify-center px-4">
        <div className="flex grid grid-cols-4 gap-x-2">
          <TRCard title="Clicks" value={900} color = "indigo"/>
          <TRCard title="Users" value={500} color = "red" />
          <TRCard title="Max Concurrency" value={800} color="cyan"/>
          <TRCard title="Average Performance" value={3.2} color = "yellow"/>          
        </div>
        <div className="flex grid py-2 gap-x-2 gap-y-0 grid-cols-3 dark:bg-neutral-900">
          <LineChartComponent/>
          <BarChartComponent />
          <AreaChartComponent />
          <ScatterChartComponent />
          <AreaLineChartComponent /> 
          {/* <TRGroupBar /> */}
        <PieChartComponent />
        </div>
      {/*<RadarChartComponent />
      */}
      </div>
    </div>
  );
};

export default Summary;
