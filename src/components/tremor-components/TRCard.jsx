import { Card, Metric, Text } from '@tremor/react';

export function TRCard({title, value, color}) {
  return (
    <Card
      className="shadow shadow-s hover:shadow-md dark:hover:shadow-lg"
      decoration="top"
      decorationColor={color}
    >
    <p className="text-tremor-default text-tremor-content dark:text-dark-tremor-content">{title}</p>
    <p className="text-3xl text-tremor-content-strong dark:text-dark-tremor-content-strong font-semibold">{value}</p>
    </Card>
  );
}