import React from "react";
import { BarChart } from "@tremor/react";

const TRGroupBar = () => {
  const chartdata = [
    {
      date: "Jan 23",
      "Clicks": 167,
      "Page loads": 145,
      "Open Water Swimming": 135,
      "Hatha Yoga": 115,
      "Street Basketball": 150,
    },
    {
      date: "Feb 23",
      "Clicks": 125,
      "Page loads": 110,
      "Open Water Swimming": 155,
      "Hatha Yoga": 85,
      "Street Basketball": 180,
    },
    {
      date: "Mar 23",
      "Clicks": 156,
      "Page loads": 149,
      "Open Water Swimming": 145,
      "Hatha Yoga": 90,
      "Street Basketball": 130,
    },
    {
      date: "Apr 23",
      "Clicks": 165,
      "Page loads": 112,
      "Open Water Swimming": 125,
      "Hatha Yoga": 105,
      "Street Basketball": 170,
    },
    {
      date: "May 23",
      "Clicks": 153,
      "Page loads": 138,
      "Open Water Swimming": 165,
      "Hatha Yoga": 100,
      "Street Basketball": 110,
    },
    {
      date: "Jun 23",
      "Clicks": 124,
      "Page loads": 145,
      "Open Water Swimming": 175,
      "Hatha Yoga": 75,
      "Street Basketball": 140,
    },
  ];

  return (
    <>
      <div
        className="flex items-center mx-auto pt-4 p-3 m-2 mb-8 shadow-outline rounded-lg dark:bg-white dark:text-black border shadow-md"
      >
        <BarChart
          className="h-72"
          data={chartdata}
          index="date"
          categories={[
            "Clicks",
            "Page loads",
            "Open Water Swimming",
          ]}
          colors={["indigo-300", "rose-200", "#ffcc33"]}
          yAxisWidth={30}
        />
      </div>
    </>
  );
};

export default TRGroupBar;
