// BarChartComponent.js
import React from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const BarChartComponent = () => {
  const data = [
    { name: 'Jan', value: 400 },
    { name: 'Feb', value: 250 },
    { name: 'Mar', value: 600 },
    { name: 'Apr', value: 800 },
    { name: 'May', value: 700 },
    { name: 'Jun', value: 900 },
    { name: 'Jul', value: 400 },
    { name: 'Aug', value: 600 },
    { name: 'Jan', value: 400 },
    { name: 'Feb', value: 250 },
    { name: 'Mar', value: 600 },
    { name: 'Apr', value: 800 },
    { name: 'May', value: 700 },
    { name: 'Jun', value: 900 },
    { name: 'Jul', value: 400 },
    { name: 'Aug', value: 600 },
    { name: 'Jan', value: 400 },
    { name: 'Feb', value: 250 },
    { name: 'Mar', value: 600 },
    { name: 'Apr', value: 800 },
    { name: 'May', value: 700 },
    { name: 'Jun', value: 900 },
    { name: 'Jul', value: 400 },
    { name: 'Aug', value: 600 },
    { name: 'Jan', value: 400 },
    { name: 'Feb', value: 250 },
    { name: 'Mar', value: 600 },
    { name: 'Apr', value: 800 },
    { name: 'May', value: 700 },
    { name: 'Jun', value: 900 },
    { name: 'Jul', value: 400 },
    { name: 'Aug', value: 600 },

  ];

  return (
<ResponsiveContainer
          className="flex items-center mx-auto dark:bg-neutral-900 pt-4 p-3 m-2 mb-8 dark:text-black shadow-outline rounded-lg border shadow-xs"
          width="100%"
          height={250}
        >      <BarChart data={data}>
        <CartesianGrid strokeDasharray="" />
        <XAxis dataKey="name"  />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="value" fill="#0976d8" />
      </BarChart>
    </ResponsiveContainer>
  );
}

export default BarChartComponent;
