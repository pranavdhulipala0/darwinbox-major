// PieChartComponent.js
import React from 'react';
import { PieChart, Pie, Tooltip, ResponsiveContainer } from 'recharts';

const PieChartComponent = () => {
  const data = [
    { name: 'Group A', value: 400 },
    { name: 'Group B', value: 250 },
    { name: 'Group C', value: 250 },
    { name: 'Group D', value: 200 },
  ];

  return (
<ResponsiveContainer
          className="flex items-center mx-auto dark:bg-neutral-900 pt-4 p-3 m-2 mb-8 shadow-outline rounded-lg border shadow-xs"
          width="100%"
          height={250}
        >      <PieChart>
        <Pie dataKey="value" isAnimationActive={true} data={data} cx="50%" cy="50%" outerRadius={150} fill="#0976d8" label />
        <Tooltip />
      </PieChart>
    </ResponsiveContainer>
  );
}

export default PieChartComponent;
    