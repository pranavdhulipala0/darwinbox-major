// FunnelChartComponent.js
import React from 'react';
import { FunnelChart, Funnel, Tooltip, ResponsiveContainer } from 'recharts';

const FunnelChartComponent = () => {
  const data = [
    { name: 'Visit', value: 1000 },
    { name: 'Register', value: 800 },
    { name: 'Paid', value: 600 },
    { name: 'Active', value: 400 },
  ];

  return (
<ResponsiveContainer
          className="flex items-center mx-auto dark:bg-neutral-900 pt-4 p-3 m-2 mb-8 shadow-outline rounded-lg border shadow-xs"
          width="100%"
          height={250}
        >      <FunnelChart>
        <Tooltip />
        <Funnel dataKey="value" data={data} />
      </FunnelChart>
    </ResponsiveContainer>
  );
}

export default FunnelChartComponent;
