"use client"
import React, { useState } from "react";
import { useRouter } from "next/navigation";
import { Sidebar } from "flowbite-react";
import { MoonIcon, SunIcon } from "@radix-ui/react-icons";
import {
  HiArrowSmRight,
  HiChartPie,
  HiInbox,
  HiShoppingBag,
  HiTable,
  HiUser,
  HiViewBoards,
} from "react-icons/hi";

const FBSidebar = () => {
  const router = useRouter(); // Get the current router object
  const [sidebarOpen, setSidebarOpen] = useState(true);

  const items = [
    {
      title: "Dashboard",
      icon: HiViewBoards,
      path: "/dashboard",
    },
    {
      title: "Event Metrics",
      icon: HiChartPie,
      path: "/events",
    },
    {
      title: "Summary",
      icon: HiShoppingBag,
      path: "/integrations",
    },
    {
      title: "Reports",
      icon: HiTable,
      path: "/reports",
    },
    {
      title: "Settings",
      icon: HiViewBoards,
      path: "/settings",
    },
  ];

  return (
    <div className="relative dark:bg-black">
      <div
        className={`p-2 my-2 rounded-r-lg bg-white shadow-md dark:bg-slate-800 transition-transform  duration-350 ${
          sidebarOpen ? "translate-x-0" : "-translate-x-full"
        }`}
      >
        <Sidebar
          aria-label="Default sidebar example"
          className="h-full dark:bg-slate-800"
        >
          <Sidebar.Items className="dark:bg-slate-800">
            <Sidebar.ItemGroup>
              {items.map((item, index) => (
                <Sidebar.Item
                  key={index}
                  className={`cursor-pointer  text-sm ${
                    router.pathname === item.path ? "bg-blue-500 text-white" : ""
                  }`}
                  href={item.path}
                  icon={item.icon}
                  size="xs"
                >
                  {item.title}
                </Sidebar.Item>
              ))}
            </Sidebar.ItemGroup>
          </Sidebar.Items>
        </Sidebar>

        {/* <div className="flex pt-4 px-3 pb-4">
          <p
            className="cursor-pointer text-sm text-gray-600 dark:text-gray-400 hover:text-gray-900 dark:hover:text-white"
            onClick={() => {
              dark ? setTheme("light") : setTheme("dark");
              setDark(!dark);
            }}
          >
            Change theme
          </p>
        </div> */}
      </div>

     
    </div>
  );
};

export default FBSidebar;
