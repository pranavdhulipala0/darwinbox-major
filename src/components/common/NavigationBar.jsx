"use client";
import { Switch } from "@tremor/react";
import {
  Avatar,
  Dropdown,
  DropdownDivider,
  DropdownHeader,
  DropdownItem,
  Navbar,
  NavbarBrand,
  NavbarCollapse,
  NavbarLink,
  NavbarToggle,
} from "flowbite-react";
import { useTheme } from "next-themes";
import { RiArrowDownSLine } from "react-icons/ri";

import CustomModal from "../elements/CustomModal";
import { useState } from "react";

export function NavigationBar() {
  const { setTheme } = useTheme();
  const [dark, setDark] = useState(false);
  return (
    <Navbar
      fluid
      rounded
      className="bg-white rounded-md hover:text-dark dark:bg-neutral-900 p-4 py-4 my-2 mx-2"
    >
      <NavbarCollapse className="">
        <NavbarLink href="#" className="text-dark flex font-semibold">
          {/* <p className="flex my-auto"> DBOX-11</p>
          <div className="px-4">
            <ScnButton buttonText="Switch client" buttonSize="xs" />
            <p className="text-semibold underline text-blue-400">Switch client</p>
          </div> */}
          <div className="flex flex-cols-2 gap-x-2 items-center">
            <CustomModal
              isOpen={true}
              modalHeading="DBOX-11"
              modalContent={<SwitchClients />}
              submitText="Switch client"
              onClose={() => {}}
              onSubmit={() => {}}
            />
            <RiArrowDownSLine />
          </div>
        </NavbarLink>
      </NavbarCollapse>
      <div className="flex md:order-2 ">
        <Dropdown
          arrowIcon={false}
          className="dark:bg-neutral-900"
          inline
          label={
            <Avatar
              alt="User settings"
              img="https://w7.pngwing.com/pngs/340/946/png-transparent-avatar-user-computer-icons-software-developer-avatar-child-face-heroes-thumbnail.png"
              rounded
            />
          }
        >
          <DropdownHeader>
            <span className="block text-sm font-semibold">Hey Drake 👋</span>
            <span className="block truncate text-sm">drakeswd@gmail.com</span>
          </DropdownHeader>
          <DropdownItem>Account</DropdownItem>
          <DropdownItem>Settings</DropdownItem>
          <DropdownItem
            onClick={() => {
              dark ? setTheme("light") : setTheme("dark");
              setDark(!dark);
            }}
          >
            Toggle mode
          </DropdownItem>
          <DropdownDivider />
          <DropdownItem>Sign out</DropdownItem>
        </Dropdown>
        <NavbarToggle />
      </div>
    </Navbar>
  );
}

const SwitchClients = () => {
  return (
    <div>
      Here, we will provide a search bar, a list of clients to select from. When
      they select this client, we will update our Client Context and render the
      data of the selected client.
    </div>
  );
};
