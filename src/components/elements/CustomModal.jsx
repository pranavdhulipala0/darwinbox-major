// CustomModal.jsx
import React from "react";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import ScnButton from "../scn/ScnButton";

const CustomModal = ({
  isOpen,
  modalHeading,
  modalContent,
  submitText,
  onSubmit,
  onClose,
}) => {
  const handleSubmit = () => {
    onSubmit();
    onClose(); // Close the modal after submitting
  };

  return (
    <Dialog isOpen={isOpen}>
      <DialogTrigger>{modalHeading}</DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>{modalHeading}</DialogTitle>
          <DialogDescription></DialogDescription>
        </DialogHeader>
        <div>{modalContent}</div>
        <div className="flex flex-cols-2 gap-x-2 justify-end">
        <ScnButton className = "p-1" color="blue" buttonText = {submitText} buttonSize = "md" buttonAction={handleSubmit}/>
        </div>

      </DialogContent>
    </Dialog>
  );
};

export default CustomModal;
