import React from "react";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";

const ScnCard = () => {
  return (
    <div >
      <Card className="dark:bg-neutral-900 border border-md dark:border-gray-700">
        <CardHeader>
          <CardTitle className="hover:text-gray-700 hover:cursor-pointer">Daily Clicks</CardTitle>
          <CardDescription>Updating..</CardDescription>
        </CardHeader>
        <CardContent>
          <p>1002</p>
        </CardContent>
      </Card>
    </div>
  );
};

export default ScnCard;
