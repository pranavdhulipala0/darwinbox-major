import { Button } from "@/components/ui/button";

const ScnButton = ({ color, className, buttonText, buttonAction, buttonSize }) => {
  const textSize = `text-${buttonSize}`;
  const buttonColor = `px-2 py-1 text-center dark:bg-blue-400 dark:text-gray-800 bg-neutral-800 text-white rounded-md bg-${color}-600`;

  return (
    <div className={buttonColor} onClick={buttonAction}>
      <Button variant="dark" className = {className} size={buttonSize ? buttonSize : "md"}>
        <p className={textSize}>{buttonText}</p>
      </Button>
    </div>
  );
};

export default ScnButton;
