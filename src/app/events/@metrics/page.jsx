"use client";
import React, { useState } from "react";
import CustomModal from "@/components/elements/CustomModal";
import TRGroupBar from "@/components/tremor-charts/TRGroupBar";
import AreaLineChartComponent from "@/components/recharts/AreaLIneChartComponent";
import { Navbar, NavbarCollapse, NavbarLink } from "flowbite-react";

const App = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleSubmit = () => {
    // Submit logic
    setIsModalOpen(false); // Close the modal after submitting
  };

  return (
    <div>
      <Navbar
        fluid
        rounded
        className="bg-white rounded-md hover:text-dark dark:bg-neutral-900 py-4"
      >
        <NavbarCollapse>
          <NavbarLink href="#" className="text-dark font-semibold">
            Clicks
          </NavbarLink>
          <NavbarLink href="#">Loads</NavbarLink>
          <NavbarLink href="#">Unique User Sessions</NavbarLink>
        </NavbarCollapse>
      </Navbar>

      <p className="text-center pt-10">We render graphs and analytics here.</p>
      <p className="text-center pt-2 pb-10">
        We provide option for user to filter data by choosing dates, modules,
        event type, users.
      </p>
    </div>
  );
};

const Component = () => {
  return (
    <div>
      {/* <TRGroupBar /> */}
      <AreaLineChartComponent />
    </div>
  );
};

export default App;
