"use client";
import AreaChartComponent from "@/components/recharts/AreaChartComponent";
import AreaLineChartComponent from "@/components/recharts/AreaLIneChartComponent";
import BarChartComponent from "@/components/recharts/BarChartComponent";
import LineChartComponent from "@/components/recharts/LineChartComponent";
import PieChartComponent from "@/components/recharts/PieChartComponent";
import ScatterChartComponent from "@/components/recharts/ScatterChartComponent";
import React from "react";
import TRGroupBar from "@/components/tremor-charts/TRGroupBar";
import { TRCard } from "@/components/tremor-components/TRCard";
import { Navbar, NavbarCollapse, NavbarLink } from "flowbite-react";

const Summary = () => {
  return (
    <>
      <Navbar
        fluid
        rounded
        className="bg-white rounded-md hover:text-dark dark:bg-neutral-900 p-4 py-6 mx-2"
      >
        <NavbarCollapse>
          <NavbarLink href="#" className="text-dark font-semibold">
            Insert features here for summarizations
          </NavbarLink>
          <NavbarLink href="#">Loads</NavbarLink>
          <NavbarLink href="#">Clicks</NavbarLink>
          <NavbarLink href="#">Unique sessions</NavbarLink>
          <NavbarLink href="#">Download summary</NavbarLink>
        </NavbarCollapse>
      </Navbar>

      <div className="flex grid grid-cols-4 gap-x-2">
        <TRCard title="Clicks" value={900} color="indigo" />
        <TRCard title="Users" value={500} color="red" />
        <TRCard title="Max Concurrency" value={800} color="cyan" />
        <TRCard title="Average Performance" value={3.2} color="yellow" />
      </div>

      <div className="flex grid py-2 gap-x-2 gap-y-0 grid-cols-3 dark:bg-neutral-900">
        <LineChartComponent />
        <BarChartComponent />
        <AreaChartComponent />
        <ScatterChartComponent />
        <AreaLineChartComponent />
        {/* <TRGroupBar /> */}
        <PieChartComponent />
      </div>
      {/*<RadarChartComponent />
       */}
    </>
  );
};

export default Summary;
