export const metadata = {
  title: "Darwinsight",
  description: "Analytics simplified.",
};

export default function DashboardLayout({ children, analytics }) {
  return (
    <div>
      {children}
      <div className="h-full w-full">
        <div className="py-1 rounded-lg bg-white dark:bg-neutral-900 p-4 mx-2 justify-center px-4">
          {analytics}
        </div>
      </div>
    </div>
  );
}
