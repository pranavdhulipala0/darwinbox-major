import { Inter } from "next/font/google";
import "./globals.css";
import { ThemeProvider } from "@/components/theme-provider";
import FBSidebar from "@/components/common/FBSidebar";
import { Toaster } from "sonner";
import {
  ResizableHandle,
  ResizablePanel,
  ResizablePanelGroup,
} from "@/components/ui/resizable";
import { NavigationBar } from "@/components/common/NavigationBar";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Darwinsight",
  description: "Analytics simplified.",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en" suppressHydrationWarning>
      <body className={inter.className}>
        <ThemeProvider
          attribute="class"
          defaultTheme="light"
          enableSystem
          disableTransitionOnChange
        >
          <div className="dark:bg-black">
            <ResizablePanelGroup direction="horizontal">
              <Toaster className="p-3 bg-blue-700" />
              <ResizablePanel defaultSize={15}>
                <FBSidebar />
              </ResizablePanel>
              <ResizablePanel defaultSize={100}>
                <div className="my-2 ml-2">
                  <NavigationBar />
                  {children}
                </div>
              </ResizablePanel>
            </ResizablePanelGroup>
          </div>
        </ThemeProvider>
      </body>
    </html>
  );
}
